import io.woo.htmltopdf.HtmlToPdf;
import io.woo.htmltopdf.HtmlToPdfException;
import io.woo.htmltopdf.HtmlToPdfObject;
import lombok.SneakyThrows;
import org.apache.pdfbox.io.MemoryUsageSetting;
import org.apache.pdfbox.multipdf.Overlay;
import org.apache.pdfbox.multipdf.PDFMergerUtility;
import org.apache.pdfbox.pdmodel.PDDocument;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Ilya Alterovych
 */
public class PdfGenerator {
  private static final String INPUT_PREFIX = "input/";
  private static final String OUTPUT_PREFIX = "output/";

  public void addWatermark(String pdfBodyString) {
    HtmlToPdf htmlToPdf = HtmlToPdf.create().object(getHtmlToPdfObject(pdfBodyString));
    try (InputStream originalInputStream = htmlToPdf.convert();
        InputStream watermarkPdf = new FileInputStream(new File(withInputPrefix("watermark.pdf")))) {
      // mergePdf(originalInputStream, watermarkPdf);
      addWatermark(originalInputStream, watermarkPdf);
    } catch (HtmlToPdfException | IOException e) {
      e.printStackTrace();
    }
  }

  public void generatePdf(String pdfBodyString) {
    boolean success = HtmlToPdf.create()
        .object(getHtmlToPdfObject(pdfBodyString))
        .convert(withOutputPrefix("pdfFile.pdf"));
  }

  @SneakyThrows
  public void mergePdf(InputStream... pdfFiles) {
    List<InputStream> list = Stream.of(pdfFiles).collect(Collectors.toList());
    PDFMergerUtility ut = new PDFMergerUtility();
    ut.addSources(list);
    ut.setDestinationFileName(withOutputPrefix("mergedHtmlFile.pdf"));
    ut.mergeDocuments(MemoryUsageSetting.setupMainMemoryOnly());
  }

  private static HtmlToPdfObject getHtmlToPdfObject(String pdfBodyString) {
    return HtmlToPdfObject.forHtml(pdfBodyString)
        .headerHtmlUrl(withInputPrefix("header.th.html"))
        .headerSpacing(4)
        .footerHtmlUrl(withInputPrefix("footer.th.html"))
        .footerSpacing(4);
  }

  @SneakyThrows
  private static String getAbsolutePath(String outputPrefix) {
    Path path = Paths.get(outputPrefix);
    if (!Files.exists(path)) {
      Files.createDirectory(path);
    }
    return path.toAbsolutePath().toString() + "/";
  }

  @SneakyThrows
  private static void addWatermark(InputStream originalInputStream, InputStream watermarkInputStream) {
    PDDocument originalPdf = PDDocument.load(originalInputStream);
    PDDocument watermarkPdf = PDDocument.load(watermarkInputStream);

    Overlay overlay = new Overlay();
    overlay.setInputPDF(originalPdf);
    overlay.setAllPagesOverlayPDF(watermarkPdf);
    overlay.overlay(new HashMap<>());
    originalPdf.save(withOutputPrefix("watermarkedPdf.pdf"));
    overlay.close();
  }

  private static String withInputPrefix(String fileName) {
    return getAbsolutePath(INPUT_PREFIX) + fileName;
  }

  private static String withOutputPrefix(String fileName) {
    return getAbsolutePath(OUTPUT_PREFIX) + fileName;
  }

}
