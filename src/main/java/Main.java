/**
 * @author Ilya Alterovych
 */
public class Main {

  public static void main(String[] args) {
    PdfGenerator pdfGenerator = new PdfGenerator();
    pdfGenerator.generatePdf(Utils.PDF_BODY_HTML);
    pdfGenerator.addWatermark(Utils.PDF_BODY_HTML);
  }
}
