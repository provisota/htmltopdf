Use https://github.com/wooio/htmltopdf-java

This patched version of `htmltopdf-java` library is required:

https://bitbucket.org/provisota/htmltopdf/downloads/put_into_local_m2_repository.zip

mirror:
https://www.dropbox.com/s/cr0stwv5e3hc08w/put_into_local_m2_repository.zip?dl=0